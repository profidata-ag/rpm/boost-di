%undefine __cmake_in_source_build
%global debug_package %{nil}

Name:           boost-di
Version:        1.2.0
Release:        2%{?dist}
Summary:        C++14 Dependency Injection Library
Group:          Development/Libraries

License:        BSD
URL:            https://github.com/boost-ext/di
Source0:        %{url}/archive/v%{version}.tar.gz
Patch0:         di_no_error_on_warning.patch
Patch1:         di_fix_failing_test.patch

%if 0%{?rhel}
BuildRequires:  gcc-toolset-10
%else
BuildRequires:  gcc-c++
%endif
BuildRequires: cmake

%description
Your C++14 header only Dependency Injection library with no dependencies

%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries

%description    devel
This package contains the header file for using %{name}.

%prep
%autosetup -p1 -n di-%{version}


%build
%if 0%{?rhel}
scl enable gcc-toolset-10 - << \SCLEOF
set -e
%endif
%cmake
%cmake_build
%if 0%{?rhel}
SCLEOF
%endif

%check
%ctest

%install
install -d "${RPM_BUILD_ROOT}%{_includedir}/boost/di"
install -m 644 -t "${RPM_BUILD_ROOT}%{_includedir}/boost" include/boost/di.hpp
cd include
find boost/di -type f -exec install -Dm 644 "{}" "${RPM_BUILD_ROOT}%{_includedir}/{}" \;
cd ../extension/include
find boost/di -type f -exec install -Dm 644 "{}" "${RPM_BUILD_ROOT}%{_includedir}/{}" \;

%files devel
%{_includedir}/boost/di.hpp
%{_includedir}/boost/di/

%changelog
* Sat Jul  3 2021 Levent Demirörs <levent.demiroers@profidata.com> - 1.2.0-2
- Add patch files for failing tests and -Werror from zagor
- Use GCC 10 when compiling for RHEL 8
